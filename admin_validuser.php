<?php
if(session_id()=='') 
	session_start();
if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}

if($_SESSION['niv_cpte']==0)
{
	header("Location: ./saved_researches.php");
}

?>

<?php
include ('./inclusions/menu.php');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<!-- Chosen CSS -->
	<link rel="stylesheet" href="./libraries/chosen_v1.4.2/chosen.css" />

	<!-- InteropProliphyc CSS -->
	<link rel="stylesheet" href="style/style.css" />

	<title>Radiomics Enabler</title>

	
	<!-- Jquery -->
	<script src="./libraries/data_table/media/js/jquery.js" type="text/javascript"></script>

	<!-- Chosen -->
	<script src="./libraries/chosen_v1.4.2/chosen.jquery.js" type="text/javascript"></script>
	<script src="./libraries/chosen_v1.4.2/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

	<!-- Fonction de vérification du formulaire -->
	<script src="./inclusions/validation.js" type="text/javascript"></script>
	<script type="text/javascript">

		jQuery(document).ready(function() {
			jQuery(".chosen").chosen();
		});
	</script>

	<!-- CustomAlert -->
	<script src="./libraries/CustomAlert/alert.js" type="text/javascript"></script>

</head>
<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<?php

	$login=$_POST["login_user"];
	$pwd=$_POST["pwd_user"];
	$lname=$_POST["lname_user"];
	$fname=$_POST["fname_user"];
	$mail=$_POST["mail_user"];
	$tel=$_POST["tel_user"];
	$genid=$_POST["genid_user"];
	$service=$_POST["service_user"];
	$priv=$_POST["priv_user"];
	$id_user = $_POST["id_user"];

	include ('./inclusions/connect.inc');
	/* Modification du jeu de résultats en utf8 */
	if (!mysqli_set_charset($conn, "utf8")) {
			//printf("Erreur lors du chargement du jeu de caractères utf8 : %s\n", mysqli_error($link));
	} else {
			//printf("Jeu de caractères courant : %s\n", mysqli_character_set_name($conn));
	}

	echo '<div class="content">';

	if ($_POST['send'] == "Enregistrer l'utilisateur") {

		$sql = 'INSERT INTO users (login, mdp, mail_user, lname_user, fname_user, level_user, genid_user, tel_user) 

		VALUES ("'.$login.'", "'.$pwd.'", "'.$mail.'", "'.$lname.'", "'.$fname.'",  "'.$priv.'", "'.$genid.'", "'.$tel.'");';

		if (mysqli_query($conn, $sql)) {

			$sql2 = 'SELECT id_user FROM users ORDER BY id_user DESC LIMIT 1';

			$result2 = mysqli_query($conn, $sql2);

			if (mysqli_num_rows($result2) > 0) {

				while($row = mysqli_fetch_assoc($result2)) {

					$id_user=$row["id_user"];		
				}

				foreach ($service as $value) {

					$sql3 = 'INSERT INTO U_S (id_user, id_service)

					VALUES ("'.$id_user.'", "'.$value.'");';

					if (mysqli_query($conn, $sql3)) {

					} else {
						echo "Error: " . $sql3 . "<br>" . mysqli_error($conn);
					}
				}

				echo "<h1> Enregistrement effectué <h1>";

				echo "<h3> L'utilisateur a été ajouté à la base de données du logiciel.";

			} else {
				echo "Error: " . $sql . "<br>" . mysqli_error($conn);
			}
		}
	} elseif ($_POST['send'] == "Modifier l'utilisateur") {

		$sql = 'UPDATE users
		SET users.login = "'.$login.'", users.mdp = "'.$pwd.'", users.mail_user = "'.$mail.'", users.lname_user = "'.$lname.'", users.fname_user = "'.$fname.'", users.fname_user = "'.$fname.'", users.level_user = "'.$level.'", users.genid_user = "'.$genid.'", users.tel_user = "'.$tel.'"
		WHERE users.id_user = '.$id_user.';';

		echo $sql;

		if (mysqli_query($conn, $sql)) {

		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}

		$sql2 = 'DELETE FROM U_S WHERE U_S.id_user = "'.$id_user.'";';

		if (mysqli_query($conn, $sql2)) {

		} else {
			echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
		}

		foreach ($service as $value) {

			$sql3 = 'INSERT INTO U_S (id_user, id_service)

			VALUES ("'.$id_user.'", "'.$value.'");';

			if (mysqli_query($conn, $sql3)) {

			} else {
				echo "Error: " . $sql3 . "<br>" . mysqli_error($conn);
			}
		}

		echo "<h1> Modification effectuée<h1>";

		echo "<h3> L'utilisateur a été modifié. </h3>";

	}elseif ($_POST['del'] == "Désactiver") {

		$sql = 'UPDATE users
		SET  users.level_user = 9
		WHERE users.id_user = '.$id_user.';';

		if (mysqli_query($conn, $sql)) {

		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}

		echo "<h1> Modification effectuée<h1>";

		echo "<h3> L'utilisateur a été désactivé. </h3>";

	}

	mysqli_close($conn)

	?>
	<center><a href="./admin_user.php" class="myButton">Retour</a></center>

</div>

</body>
</html>