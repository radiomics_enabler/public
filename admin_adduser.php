<?php
if(session_id()=='') 
	session_start();
if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}

if($_SESSION['niv_cpte']==0)
{
	header("Location: ./saved_researches.php");
}

?>

<?php
include ('./inclusions/menu.php');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<!-- Chosen CSS -->
	<link rel="stylesheet" href="./libraries/chosen_v1.4.2/chosen.css" />

	<!-- InteropProliphyc CSS -->
	<link rel="stylesheet" href="style/style.css" />

	<title>Radiomics Enabler</title>

	
	<!-- Jquery -->
	<script src="./libraries/data_table/media/js/jquery.js" type="text/javascript"></script>

	<!-- Chosen -->
	<script src="./libraries/chosen_v1.4.2/chosen.jquery.js" type="text/javascript"></script>
	<script src="./libraries/chosen_v1.4.2/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

	<!-- Fonction de vérification du formulaire -->
	<script src="./inclusions/validation.js" type="text/javascript"></script>
	<script type="text/javascript">

		jQuery(document).ready(function() {
			jQuery(".chosen").chosen();
		});
	</script>

	<!-- CustomAlert -->
	<script src="./libraries/CustomAlert/alert.js" type="text/javascript"></script>

</head>
<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<div class="content">

		<h1>Ajouter un utilisateur :</h1>

		<br/>

		<!-- Formulaire de saisie des données -->

		<center>
			<table>
				<form name="frm" method="POST" action="admin_validuser.php"> 
					<tr>
						<td>
							<fieldset>
								<legend>
									Informations de connexion :
								</legend>
								<br/>
								Login :
								<input type="text" size=15 name="login_user">
								<br/>
								<br/>
								Mot de passe :
								<input type="text" size=15 name="pwd_user">
								<br/>
								<br/>
								<br/>

							</fieldset></td>
							<td> </td>
							<td>
								<fieldset>
									<legend>
										Informations générales :
									</legend>
									<br/>
									Nom :
									<input type="text" size=15 name="lname_user">
									<br/>
									<br/>
									Prénom :
									<input type="text" size=30 name="fname_user">
									<br/>
									<br/>
									Adresse e-mail :
									<input type="text" size=30 name="mail_user">
									<br/>
									<br/>
									Numéro de téléphone (optionnel) :
									<input type="text" size=30 name="tel_user">
									<br/>
									<br/>
									Numéro RPPS (optionnel) :
									<input type="text" size=30 name="genid_user">
									<br/>
									<br/>
									Service: 
									<select style = "width:50%" name = "service_user[]" size=30 data-placeholder="..." multiple class="chosen" tabindex="100">

										<?php 

										include ('./inclusions/connect.inc');

										if (!mysqli_set_charset($conn, "utf8")) {
											//printf("Erreur lors du chargement du jeu de caractères utf8 : %s\n", mysqli_error($link));
										} else {
											//printf("Jeu de caractères courant : %s\n", mysqli_character_set_name($conn));
										}

										$sql = 'SELECT * FROM services';

										$results = (mysqli_query($conn, $sql));

										if (mysqli_num_rows($results) > 0) {

											while($row = mysqli_fetch_assoc($results)) { ?>

											<option value="<?php echo $row['id_service'] ?>"> <?php echo $row['name_service'] ?> </option>
											<?php
										} }

										?>
									</select>

								</fieldset></td>
							</tr>
						</table>
						<br/>
						<br/>

						<fieldset style="width:60%;">
							<legend>
								Priviligès de l'utilisateur :
							</legend>
							<br>


							Priviligès :

							<select style = "width:50%" name = "priv_user" size=30 data-placeholder="..." class="chosen" tabindex="100">

								<option value=""></option>
								<option value ="1">Droits administrateur</option>
								<option value ="0">Utilisateur classique</option>

							</select><span class="espace"/>

						</fieldset>
						<br/>
						<br/>

						<?php

						mysqli_close($conn)

						?>

						<center>
							<input border=0 type="submit" name="send" value="Enregistrer l'utilisateur" class="myButton"> <span class="espace"/> <a href="./admin_user.php" class="myButton">Retour</a>
						</center>

					</center>
				</form>
				<br/>
			</div>
		</body>
		</html>