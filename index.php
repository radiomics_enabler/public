<?php
if(session_id()=='') 
	session_start();
if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<!-- Chosen CSS -->
	<link rel="stylesheet" href="./libraries/chosen_v1.4.2/chosen.css" />

	<!-- InteropProliphyc CSS -->
	<link rel="stylesheet" href="style/style.css" />

	<title>Radiomics Enabler</title>

	
	<!-- Jquery -->
	<script src="./libraries/data_table/media/js/jquery.js" type="text/javascript"></script>

	<!-- Chosen -->
	<script src="./libraries/chosen_v1.4.2/chosen.jquery.js" type="text/javascript"></script>
	<script src="./libraries/chosen_v1.4.2/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

	<!-- Fonction de vérification du formulaire -->
	<script src="./inclusions/validation.js" type="text/javascript"></script>
	<script type="text/javascript">

		jQuery(document).ready(function() {
			jQuery(".chosen").chosen();
		});
	</script>

	<!-- CustomAlert -->
	<script src="./libraries/CustomAlert/alert.js" type="text/javascript"></script>

</head>
<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<div class="content">

		<h1>Recherche libre :</h1>

		<br/>

		<!-- Formulaire de saisie des données -->

		<center>
			<table>
				<form name="frm" method="POST" action="find_patient.php" onSubmit="return valider_formulaire()"> 
					<tr>
						<td>
							<fieldset>
								<legend>
									Date d'examen :
								</legend>
								<br/>
								Du :
								<input type="date" size=30 name="datedeb">
								<br/>
								<br/>
								Au :
								<input type="date" size=30 name="datefin">
								<br/>
								<br/>
								<br/>

							</fieldset></td>
							<td> OU </td>
							<td>
								<fieldset>
									<legend>
										Patient ou examen :
									</legend>
									<br/>
									Nom du patient :
									<input type="text" size=30 name="nom_patient">
									<br/>
									<br/>
									Né entre le :
									<input type="date" size=30 name="datenaissdeb_patient">
									Et le :
									<input type="date" size=30 name="datenaissfin_patient">
									<br/>
									<br/>
									Identifiant du patient :
									<input type="text" size=30 name="id_patient">
									<br/>
									<br/>
									Numéro de demande:
									<input type="text" size=30 name="num_dos">
									<br/>
									<br/>
								</fieldset></td>
							</tr>
						</table>
						<br/>
						<br/>

						<fieldset style="width:60%;">
							<legend>
								Critères additionnels :
							</legend>
							<br>


							Modalité :
							<select style = "width:10%" name = "modalite[]" size=7 data-placeholder="..." multiple class="chosen" tabindex="100">
								<option value=""></option>
								<option value ="NM">NM</option>
								<option value ="PT">PT</option>
								<option value ="KO">KO</option>
								<option value ="MR">MR</option>
								<option value ="CT">CT</option>
								<option value ="XA">XA</option>
							</select><span class="espace"/>
							
							<a class="infobulle">Type(s) d'examen :
								<span class="infobulle"><u>Recherche par type(s) d'examen</u> : <br/>
									Vous pouvez effectuer une recherche sur plusieurs expressions. <br/>
									Pour ce faire, séparez les différentes expressions par des points-virgules. <br/>
									Exemple : cerveau datscan;poumon;rein</span>
								</a>
								
								<textarea name="type_ex" rows =4 cols=40 style="max-width:50%"></textarea>

								

							</fieldset>
							<br/>
							<br/>

							<center>
								<input border=0 type="submit" value="Rechercher" class="myButton">
							</center>

						</center>
					</form>
					<br/>
				</div>
			</body>
			</html>