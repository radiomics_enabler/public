function valider_formulaire() {
	var c1 = document.frm["datedeb"].value;

	var c2 = document.frm["datefin"].value;

	var c3 = document.frm["nom_patient"].value;

	var c4 = document.frm["datenaissdeb_patient"].value;

	var c5 = document.frm["datenaissfin_patient"].value;

	var c6 = document.frm["id_patient"].value;

	var c7 = document.frm["num_dos"].value;

	if ((c1 == "") && (c2 == "") && (c3 == "") && (c4 == "") && (c5 == "") && (c6 == "") && (c7 == "")) {
		alert('Veuillez remplir au moins un champ !', 'Erreur' );
		return false;
	}

	else if ((c1 != "") && (c2 != "") && (c1 > c2)) {
		alert('Attention ! Votre intervalle de date est incorrect !' , 'Erreur');
		return false;
	}

	else if ((c4 != "") && (c5 != "") && (c4 > c5)) {
		alert('Attention ! Votre intervalle de date de naissance est incorrect !' , 'Erreur');
		return false;
	}
	
	else {
	return true;
	
	}
}