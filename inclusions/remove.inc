<?php

function remove_file ($dossier_traite)
{

	$repertoire = opendir($dossier_traite); // On d�finit le r�pertoire dans lequel on souhaite travailler.
	while (false !== ($fichier = readdir($repertoire))) // On lit chaque fichier du r�pertoire dans la boucle.
	{
		$chemin = $dossier_traite."/".$fichier; // On d�finit le chemin du fichier � effacer.
		
		if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier))
		{
		       unlink($chemin); // On efface.
		   }
		}

		closedir($repertoire);

	}

	?>