<?php
if(session_id()=='') 
	session_start();

if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}

if($_SESSION['niv_cpte']==0)
{
	header("Location: ./saved_researches.php");
}

?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="style/style.css" />

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/extensions/TableTools/css/dataTables.tableTools.css">

	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.js"></script>

	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/extensions/TableTools/js/dataTables.tableTools.js"></script>

	<title>Radiomics Enabler</title>

	<!-- Paramétrage de DataTables -->
	<script type="text/javascript">

		$(document).ready( function () {
			$('#result').DataTable( {
				"language": {
					"url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json" // Choix du fichier langue

				},
				"order": [ 0, 'asc' ], // Paramétrage du tri par défaut (tri ascendant sur colonne 2)
				"aoColumnDefs": [
				{ "bSortable": false, "aTargets": [ 3,4 ] } // On empeche le tri dynamique sur colonne 0 (cases à cocher)
				],
				aLengthMenu: [ // Choix possble dans le menu de taille du tableau
				[10,25, 50, 100, 200, -1],
				[10,25, 50, 100, 200, "Tous"]
				],



				dom: 'T<"clear">lfrtip',

				tableTools: { // Initialisation des fonctions d'export CSV / PDF
				"sSwfPath": "//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
				{
					"sExtends": "copy",
					"sButtonText": "Copier"
				},
				{
					"sExtends": "csv",
					"sButtonText": "Export CSV"
				},
				{
					"sExtends": "pdf",
					"sButtonText": "Export PDF"
				}
				]
			}

		}
		);

			var table = $('#result').DataTable();

			$('#selectall').click(function(event) { // Fonction qui selectionne toutes les checkbox, lors d'un clic sur la checkbox dans le header du tableau :

    			if(this.checked) { // Si on coche la checkbox dans le header
          			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
          			this.checked = true;  // On les coche    
          			$(this).closest("tr").addClass("selected"); // On passe la classe de DataTable a "selected", pour obtenir l'effet visuel de sélection               
          		});
          		}

          		else{ // Si on décoche la checkbox dans le header
           			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
           			this.checked = false; // On les decoche
           			$(this).closest("tr").removeClass("selected"); // On enlève la classe de DataTable "selected", pour enlever l'effet visuel de sélection 
           		});        
           		}
           	});

			$('#result tbody').on( 'click', ':checkbox', function() {
				$(this).closest("tr").toggleClass('selected');  // Lors du cochage/décochage individuel d'une checkbox, on active/désactive la classe "selected" pour gérer les effets visuels
			} );
		} );


</script>

</head>

<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<div class="content">

		<h1>Administration - Utilisateurs Enregistrés :</h1>

		<center> <table id="result" class="display">
			<thead><tr>
				<th>Nom</th><th>Prénom</th><th>Service(s)</th><th>Modification</th><th>Suppression</th>
			</tr></thead><tbody>

			<?php

			include ('./inclusions/connect.inc');
			/* Modification du jeu de résultats en utf8 */
			if (!mysqli_set_charset($conn, "utf8")) {
			//printf("Erreur lors du chargement du jeu de caractères utf8 : %s\n", mysqli_error($link));
			} else {
			//printf("Jeu de caractères courant : %s\n", mysqli_character_set_name($conn));
			}

			$sql='SELECT users.* FROM users WHERE users.level_user != 9;';

			$result = mysqli_query($conn, $sql);

			if (mysqli_num_rows($result) > 0) {

				while($row = mysqli_fetch_assoc($result)) {

					$lname = $row["lname_user"];
					$fname = $row["fname_user"];
					$iduser = $row["id_user"];

					$sql2='SELECT U_S.*, services.* FROM U_S, services WHERE U_S.id_user = '.$iduser.' AND U_S.id_service = services.id_service;';

					$result2 = mysqli_query($conn, $sql2);

					$services = "";

					while($row2 = mysqli_fetch_assoc($result2)) {

						if ($services == "") {;

							$services = $row2['name_service'];
						}

						else {

							$services .= " / ".$row2['name_service'];

						}
					}
					


					echo '<tr>
					<td>'.$lname.'</td>
					<td>'.$fname.'</td>
					<td>'.$services.'</td>
					<td><center><form id="form1" name="moduser" method="POST" action="admin_moduser.php"> <input type="hidden" name="id_user" value="'.$iduser.'" /><input type="submit" name="mod" value="Modifier" class="myButton" ></center></form></td>
					<td><center><form id="form2" name="deluser" method="POST" action="admin_validuser.php"> <input type="hidden" name="id_user" value="'.$iduser.'" /><input type="submit" name="del" value="Désactiver" class="myButton" ></center></form></td></tr> 
					';

				}
			}
			mysqli_close($conn);

			?>
		</tbody>
	</table>

	<a href="./admin_adduser.php" class="myButton">Ajouter un nouvel utilisateur</a>

</center>
</div>
</body>
</html>