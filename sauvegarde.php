<?php
if(session_id()=='') 
	session_start();

if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="stylesheet" href="style/style.css" />

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/extensions/TableTools/css/dataTables.tableTools.css">

	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.js"></script>

	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/extensions/TableTools/js/dataTables.tableTools.js"></script>

	<title>Radiomics Enabler</title>
</head>

<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<div class="content">

		<?php

		$lib_recherche=$_POST["lib_recherche"];
		$desc_recherche=$_POST["desc_recherche"];
		$crit_nom=$_POST["crit_nom"];
		$crit_datenaissdeb=$_POST["crit_datenaissdeb"];
		$crit_datenaissfin=$_POST["crit_datenaissfin"];
		$crit_datedeb=$_POST["crit_datedeb"];
		$crit_datefin=$_POST["crit_datefin"];
		$crit_modalite=$_POST["crit_modalite"];
		$crit_id=$_POST["crit_id"];
		$crit_type=$_POST["crit_type"];
		$crit_csv=$_POST["crit_csv"];
		$type_save=$_POST["type_save"];

		include ('./inclusions/connect.inc');
		/* Modification du jeu de résultats en utf8 */
		if (!mysqli_set_charset($conn, "utf8")) {
			//printf("Erreur lors du chargement du jeu de caractères utf8 : %s\n", mysqli_error($link));
		} else {
			//printf("Jeu de caractères courant : %s\n", mysqli_character_set_name($conn));
		}


		$sql = 'INSERT INTO searches (id_user, id_project, date_search, dateb_search, datee_search, namep_crit, dobpb_crit, dobpe_crit, idp_crit, modality_crit, type_crit, csv_crit, name_search, desc_search, type_save) 

		VALUES ("'.$_SESSION["id_user"].'", NULL, CURDATE(), "'.$crit_datedeb.'", "'.$crit_datefin.'", "'.$crit_nom.'", "'.$crit_datenaissdeb.'", "'.$crit_datenaissfin.'", "'.$crit_id.'", "'.$crit_modalite.'", "'.$crit_type.'", "'.$crit_csv.'", "'.$lib_recherche.'", "'.$desc_recherche.'", "'.$type_save.'");';

		if (mysqli_query($conn, $sql)) {

			$sql2='SELECT id_search FROM searches WHERE id_user="'.$_SESSION["id_user"].'" ORDER BY id_search DESC LIMIT 1;';

			$result2 = mysqli_query($conn, $sql2);

			if (mysqli_num_rows($result2) > 0) {

				while($row = mysqli_fetch_assoc($result2)) {

					$id_recherche=$row["id_search"];		
				}


				$examens = glob('./dcm/*.{dcm}', GLOB_BRACE);
				foreach($examens as $file) {

					require_once './libraries/nanodicom-master/nanodicom.php';
					$dicom = Nanodicom::factory($file);
					$dicom -> parse(); 

					$sql3 = 'INSERT INTO exams (DICOM_STUDY_UID, name_pat, dob_pat, date_exam, modality, STUDY_DESCRIPTION, id_pat) 
					VALUES ("'.$dicom -> value(0x0020, 0x000d).'", "'.$dicom -> value(0x0010, 0x0010).'", "'.dicom_to_date($dicom -> value(0x0010, 0x0030)).'", "'.dicom_to_date($dicom -> value(0x0008, 0x0020)).'", "'.$dicom -> value(0x0008, 0x0061).'", "'.$dicom -> value(0x0032, 0x1060).'", "'.$dicom -> value(0x0010, 0x0020).'") ON DUPLICATE KEY UPDATE DICOM_STUDY_UID=DICOM_STUDY_UID;';

					if (mysqli_query($conn, $sql3)) {

						if ($type_save=="STUDIES"){

							$sql4 = 'INSERT INTO lines_r (id_search, DICOM_STUDY_UID)
							VALUES ("'.$id_recherche.'", "'.$dicom -> value(0x0020, 0x000d).'");';

							if (mysqli_query($conn, $sql4)) {

							} else {
								echo "Error: " . $sql4 . "<br>" . mysqli_error($conn);
							}
						}

					} else {
						echo "Error: " . $sql3 . "<br>" . mysqli_error($conn);
					} }

				} else {
					echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
				}

				echo "<h1>Votre recherche a été bien sauvegardée</h1>";

			} else {
				echo "Error: " . $sql . "<br>" . mysqli_error($conn);
			}

			if ($type_save ==  "SERIES"){

				$file_series = glob('./series_dcm/*.{dcm}', GLOB_BRACE);
				foreach($file_series as $file_series) {

					require_once './libraries/nanodicom-master/nanodicom.php';
					$dicom = Nanodicom::factory($file_series);
					$dicom -> parse(); 

					$sql5 = 'INSERT INTO series (SERIES_UID, DICOM_STUDY_UID, SERIES_DESC)
					VALUES ("'.$dicom -> value(0x0020, 0x000e).'", "'.$dicom -> value(0x0020, 0x000d).'", "'.$dicom -> value(0x0032, 0x1060).'");';

					if (mysqli_query($conn, $sql5)) {

						$sql6 = 'INSERT INTO lines_r (id_search, DICOM_STUDY_UID)
						VALUES ("'.$id_recherche.'", "'.$dicom -> value(0x0020, 0x000d).'")
						ON DUPLICATE KEY UPDATE id_search = id_search;';

						if (mysqli_query($conn, $sql6)) {

						} else {
							echo "Error: " . $sql6 . "<br>" . mysqli_error($conn);
						}


					} else {
						echo "Error: " . $sql5 . "<br>" . mysqli_error($conn);
					}
				}
			}

			mysqli_close($conn);

			?>

			<a href="./saved_researches.php" class="myButton">Retour</a>

		</div>
	</body>
	</html>