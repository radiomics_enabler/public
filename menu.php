<?php

function menu($niveau) {

	include ('./inclusions/dicom_server.inc');

	$requete = 'sudo /usr/bin/echoscu '. $dicom_server .' '. $dicom_port .' 2>&1 -v '; // Module de vérification de la connexion au PACS

	exec($requete, $output);

	$sortie = $output[3];

	if ($sortie == "I: Received Echo Response (Status: Success)") { // Si la troisième ligne du fichier de sortie est "status : success", la connexion a été effectuée
		$statut = "Connexion réussie";
		$image = '<img src="./images/feu_vert.png" alt="green" height="20" width="13">';
	} else { // Sinon, la connexion échoue
		$statut = "Connexion impossible";
		$image = '<img src="./images/feu_rouge.png" alt="red" height="42" width="42">';
	}

	echo '<div class="pageheader">

	Radiomics Enabler - v1.45b

</div>

<div class="etat">
	<p>' . $image . 'État de la connexion au serveur DICOM : ' . $statut . '</p>
</div>';

if ($niveau == "Principal") { // Menu principal


	echo '<div class="logo"> </div> <div class="menu"> 

	<center><p><h1>Menu <b>' . $niveau . ' : </b> </h1></p><br/></center>	

	<p><a href="./index.php" class="myMenuButton">Recherche libre</a></p>
	<p><a href="./path_csv.php" class="myMenuButton">Recherche via fichier CSV</a></p>
	<p><a href="./saved_researches.php" class="myMenuButton">Recherches sauvegardées</a></p>

	<center><p>------------------------------------------------------</p></center>

	<p><a href="./deconnexion.php" class="myMenuButton">Déconnexion</a></p><br/>
	<center><p><a href="./apropos.php" >A propos du logiciel</a></p></center>
</div> ';



} elseif ($niveau == "Connexion") { // Menu de connexion
	echo '<div class="logo"> </div>  <div class="menu">

	<center><h1><b>' . $niveau . ' : </b> </h1></center>

	<form method = "POST" action="traitement_connexion.php">

		<p>
			<br/><b style="color:#000000">Veuillez vous identifier :</b>
			<table border=0>
				<tr>
					<td><label for="nom" style="color:#000000">Login : </label></TD>
						<td><input type="text" name="zs_login" id="nom" /></TD>
						</tr>
						<tr>
							<td><label for="mdp" style="color:#000000">Mot de passe : </label></TD>
								<td><input type="password" name="zs_mdp" id="mdp" /></TD>
								</tr>
							</table><br/>
						</p>	
						<input  border=0 class="myMenuButton" type="submit" value="Connexion">
					</form>
				</div>';
			}
		}
		?>