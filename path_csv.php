<?php
ini_set('display_errors', 1);
if(session_id()=='') 
	session_start();
if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>
<?php
include ('./inclusions/menu.php');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">

	<!-- Chosen CSS -->
	<link rel="stylesheet" href="./libraries/chosen_v1.4.2/chosen.css" />

	<!-- InteropProliphyc CSS -->
	<link rel="stylesheet" href="style/style.css" />

	<title>Radiomics Enabler</title>

	<!-- Jquery -->
	<script src="./libraries/data_table/media/js/jquery.js" type="text/javascript"></script>

	<!-- Chosen -->
	<script src="./libraries/chosen_v1.4.2/chosen.jquery.js"></script>
	<script>
		jQuery(document).ready(function() {
			jQuery(".chosen").chosen();
		});
	</script>
</head>
<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<div class="content">

		<h1>Recherche via fichier CSV :</h1>

		<br/>

		<center>

			<form name="find_DICOM" method="POST" action="find_csv.php" enctype="multipart/form-data">
				<fieldset style="width:45%";>
					<legend>
						Indiquer le chemin du fichier CSV :
					</legend>
					<br/>
					<input type="file" name="file_csv" id="file_csv"/>
					<br/><br/>
						<a class="infobulle"><u>Informations sur les fichiers CSV :</u>
								<span class="infobulle">
									Le fichier CSV utilisé doit avoir les Numéros de Dossier Xplore (NumDos) des patients en tant que première colonne, 
									et utiliser le type de séparateur "point-virgule", 
									pour pouvoir être lu par l'application

								</a>
					<br/><br/>
				</fieldset>

			
				<br/>
				<br/>

				<fieldset style="width:50%;">
					<legend>
						Critères additionnels :
					</legend>
					<br>


					Modalité :
					<select style = "width:10%" name = "modalite[]" size=7 data-placeholder="..." multiple class="chosen" tabindex="100">
						<option value=""></option>
						<option value ="NM">NM</option>
						<option value ="PT">PT</option>
						<option value ="KO">KO</option>
						<option value ="MR">MR</option>
						<option value ="CT">CT</option>
						<option value ="XA">XA</option>
					</select><span class="espace"/>
					Type d'examen :
					<textarea name="type_ex" rows =4 cols=40 style="max-width:50%"></textarea>
				</fieldset>
				<br/>
				<br/>

				<center>
					<input border=0 type="submit" value="Rechercher" class="myButton">
				</center>
			</center>
		</form>
		<br/>
	</div>
</body>
</html>