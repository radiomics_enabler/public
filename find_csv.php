<?php
if(session_id()=='') 
	session_start();
	ini_set('display_errors','on');
	error_reporting(E_ALL);


if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="./style/style.css" />

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/extensions/TableTools/css/dataTables.tableTools.css">

	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.js"></script>

	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/extensions/TableTools/js/dataTables.tableTools.js"></script>

	<title>Radiomics Enabler</title>

	<!-- Paramétrage de DataTables -->
	<script type="text/javascript">

		$(document).ready( function () {
			$('#result').DataTable( {
				"language": {
					"url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json" // Choix du fichier langue

				},
				"order": [ 2, 'asc' ], // Paramétrage du tri par défaut (tri ascendant sur colonne 2)
				"aoColumnDefs": [
				{ "bSortable": false, "aTargets": [ 0 ] } // On empeche le tri dynamique sur colonne 0 (cases à cocher)
				],
				aLengthMenu: [ // Choix possble dans le menu de taille du tableau
				[10, 25, 50, 100, 200, -1],
				[10, 25, 50, 100, 200, "Tous"]
				],



				dom: 'T<"clear">lfrtip',

				tableTools: { // Initialisation des fonctions d'export CSV / PDF
				"sSwfPath": "//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
				{
					"sExtends": "copy",
					"sButtonText": "Copier"
				},
				{
					"sExtends": "csv",
					"sButtonText": "Export CSV"
				},
				{
					"sExtends": "pdf",
					"sButtonText": "Export PDF"
				}
				]
			}

		}
		);

			var table = $('#result').DataTable();

			$('#selectall').click(function(event) { // Fonction qui selectionne toutes les checkbox, lors d'un clic sur la checkbox dans le header du tableau :
    			if(this.checked) { // Si on coche la checkbox dans le header
          			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
          			this.checked = true;  // On les coche   
          			$(this).closest("tr").addClass("selected"); // On passe la classe de DataTable a "selected", pour obtenir l'effet visuel de sélection         
          		});
          		}

          		else{ // Si on décoche la checkbox dans le header
           			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
           			this.checked = false; // On les decoche
           			$(this).closest("tr").removeClass("selected");  // On enlève la classe de DataTable "selected", pour enlever l'effet visuel de sélection 
           		});        
           		}
           	});

			$('#result tbody').on( 'click', ':checkbox', function() {
				$(this).closest("tr").toggleClass('selected'); // Lors du cochage/décochage individuel d'une checkbox, on active/désactive la classe "selected" pour gérer les effets visuels
			} );
		} );


</script>


</head>

<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<div class="content">

		<h1>Résultats de la recherche :</h1>

		<?php

		if (isset($_POST['retour_csv'])) { // Si on revient sur cette page, depuis la page qui suit : (sélection des séries)

			// On récupère nos variables :

			$filtrage_type = $_POST["filtrage_type"];
			$table_type_serial = $_POST["table_type"];

			$table_type = unserialize($table_type_serial); // Déserialisation du tableau, pour permettre la lecture en tant que texte

			$nb_file = sizeof(glob("./dcm/*.dcm" )); // On compte le nombre de fichiers dans le dossier DCM

			// On regénère le tableau, sans avoir à rééexcuter la reqûete :

			if ($nb_file == 0) {
				echo '<h2>Aucun résultat n&apos;a été trouvé pour vos paramètres de recherche</h2>';
			} else {

				echo '
				<form id="ok" name="traitement" method="POST" action="traitement.php">
					<center>
						<table id="result" class="display">
							<thead><tr>
								<th>
									<input type="checkbox" id="checkall" title="Select all" onClick="toggle(this)"/>
								</th><th>Identifiant du patient</th><th>Nom du patient</th><th>Date de naissance</th><th>Date d&apos;examen</th><th>Modalité</th><th>Type d&apos;examen</th>
							</tr></thead>';

							for ($i = 1; $i <= $nb_file; $i++) { // On parse les fichiers DCM, à l'aide de l'utilitaire Nanodicom

							$nom_file = 'rsp' . $i . '.dcm';

							require_once './libraries/nanodicom-master/nanodicom.php';
							$dicom = Nanodicom::factory('./dcm/' . $nom_file);
							$dicom -> parse();

								if ($filtrage_type == 1) { // Si un filtrage sur le type a été indiqué, on le prend en comtpe :

									$string = $dicom -> value(0x0032, 0x1060);

									$table_type_ligne = count($table_type);

									$bingo = FALSE;

									for ($j = 0; $j < $table_type_ligne; $j++) { // On vérifie si le(s) mot(s) indiqués dans le champ de filtrage appartiennent au type indiqué dans le fichier DICOM

										if (stristr($string, $table_type[$j]) === FALSE) {

										} else {
											$bingo = TRUE;
										}
									}

									if ($bingo === TRUE) { // S'il y a filtrage et que les mots demandés on été trouvé, on affichage les résultats correspondant

									echo '
									<tr>
										<td>
											<input name="nom_checkbox[]" type="checkbox" value="' . $nom_file . '" />
										</td><td> ' . $dicom -> value(0x0010, 0x0020) . '</td>
										<td>' . $dicom -> value(0x0010, 0x0010) . '</td>
										<td>' . dicom_to_date($dicom -> value(0x0010, 0x0030)) . '</td>
										<td>' . dicom_to_date($dicom -> value(0x0008, 0x0020)) . '</td>
										<td>' . $dicom -> value(0x0008, 0x0061) . '</td>
										<td>' . $dicom -> value(0x0032, 0x1060) . '</td>
									</tr>';

								} 


								} else { // S'il n'y a pas de filtrage, on affiche tout

								echo '
								<tr>
									<td>
										<input name="nom_checkbox[]" type="checkbox" value="' . $nom_file . '" />
									</td><td> ' . $dicom -> value(0x0010, 0x0020) . '</td>
									<td>' . $dicom -> value(0x0010, 0x0010) . '</td>
									<td>' . dicom_to_date($dicom -> value(0x0010, 0x0030)) . '</td>
									<td>' . dicom_to_date($dicom -> value(0x0008, 0x0020)) . '</td>
									<td>' . $dicom -> value(0x0008, 0x0061) . '</td>
									<td>' . $dicom -> value(0x0032, 0x1060) . '</td>
								</tr>';

							}

						}

							// On stocke les variables importantes dans un formulaire caché, afin de les récupérer lors d'un changement de page

						echo '
					</table>
					<br/>
					<br/>
					<input type="hidden" name="filecsv" value="filecsv" />
					<input type="hidden" name="filtrage_type"  value="' . $filtrage_type . '" />
					<input type="hidden" name="table_type"  value="' . htmlentities($table_type_serial) . '" /> 

					<input border=0 type="submit" name="series" value="Afficher les séries" class="myButton">
				</center>
			</form><br/>';

				// Si un identifiant demandé n'est pas trouvé lors de la recherche : on l'affiche ici

			if (isset($table_p) && !empty($table_p)) {
				echo "<h2>Aucun résultat n'a été trouvé pour les identifiants suivants : </h2><br/>
				<center><table class='result'>";

					for ($l = 1; $l <= count($table_p)-1; $l++) {
						echo '<tr><td>' . $table_p[$l] . '</td></tr>';
					}

					echo "</table></center>"; 

				}

			}

			} else { // Si on accède à la page depuis le moteur de recherche, de manière "normale"

			$filtrage_type = 0;
			$modalite = "";
			$table_type = "";


				if (isset($_POST['type_ex']) && !empty($_POST['type_ex'])) { // Si le champ type a été renseigné :

					$table_type = explode (";", $_POST['type_ex']);
					$table_type_ligne = count($table_type);
					$filtrage_type = 1;

				} 

				$table_type_serial = serialize($table_type);



				if (isset($_POST['modalite']) && !empty($_POST['modalite'])) { // Si une ou des modalités ont été demandées :
					$ModArray = $_POST['modalite'];

					$a = count($ModArray);

					if ($a == 1) {
						$modalite = $ModArray[$a-1];

					} else {
						$modalite = $ModArray[0];
						for ($b = 1; $b < $a; $b++ ){
							$modalite = $modalite.'\\'.$ModArray[$b];
						}

					}

				}

				// Upload du fichier CSV sur le serveur :
				$target_dir = "./upload/";
				remove_file($target_dir);
				$target_file = $target_dir . basename($_FILES["file_csv"]["name"]);
				$uploadOk = 1;
				$fileType = pathinfo($target_file, PATHINFO_EXTENSION);

				if ($fileType != "csv") { // Si le fichier choisi n'est pas de type CSV
					echo "<h2>Opération impossible. Veuillez s'il vous plaît indiquer le chemin vers un fichier CSV.</h2>";
					$uploadOk = 0;
				} else {

					if ($uploadOk == 0) { // Si le fichier uploadé ne peut pas être lu :
						echo "<h2>Votre fichier n'a pas pu être utilisé. Merci de rééssayer l'opération</h2>";
					} else {
						if (move_uploaded_file($_FILES["file_csv"]["tmp_name"], $target_file)) {
							echo "<h2>Opération réussie !</h2><br/>";

							remove_file("./csv_dcm");

							remove_file("./dcm");

							$n = 0;
							$d = 1;
							$p = 0;
							$row = 1;
							if (($handle = fopen($target_file, "r")) !== FALSE) {
								while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
									$num = count($data);
									if($row == 1){ $row++; continue; }

									$requete2 = 'cd ./csv_dcm && sudo /usr/bin/findscu -X -S --aetitle PROL_QUERY_SCU --call ' . $dicom_aet . ' -k "(0008,0052)=STUDY" -k "(0020,000D)" -k "(0008,0061)= ' . $modalite . ' " -k "(0032,1060)=" -k "(0008,0020)=" -k "(0010,0010)=" -k "(0010,0020)=' . $data[0] . '" -k "(0010,0030)=" ' . $dicom_server . ' ' . $dicom_port;
									exec($requete2, $output); // On lance la requête DICOM - les fichiers .dcm se téléchargent dans le dossier csv_dcm

									$nb_dcm = sizeof(glob("./csv_dcm/*.dcm" )); // On compte le nombre de fichiers reçus par requêtes

									if ($nb_dcm == 0) { // Si aucun fichier n'est reçu pour la requête :

										$table_p[$p] = $data[0];
										$p++;

									} 

									for ($j = 1; $j <= $nb_dcm; $j++) {

										if ($j < 10) {
											$nom_file = 'rsp000' . $j . '.dcm';
										} elseif (10 <= $j && $j < 100) {
											$nom_file = 'rsp00' . $j . '.dcm';
										} elseif (100 <= $j && $j < 1000) {
											$nom_file = 'rsp0' . $j . '.dcm';
										} elseif ($j >= 1000) {
											$nom_file = 'rsp' . $j . '.dcm';
										}

										$requete4 = 'cd ./csv_dcm && sudo cp ' . $nom_file . ' ../dcm/rsp' . $d . '.dcm';
										exec($requete4, $output); // On déplace au fur et a mesure les fichiers reçus vers le dossier dcm, en les renommant sur le modèle "rsp****.dcm"
										$d++;

									}

									$n++;
									remove_file("./csv_dcm");

									
								}
								fclose($handle); 
							}

							$nb_file = sizeof(glob("./dcm/*.dcm" )); // On compte le nombre de fichiers dans le dossier dcm

							if ($nb_file == 0) {
								echo '<h2>Aucun résultat n&apos;a été trouvé pour vos paramètres de recherche</h2>';
							} else {

								echo '
								<form id="ok" name="traitement" method="POST" action="traitement.php">
									<center>
										<table id="result" class="display">
											<thead><tr>
												<th>
													<input type="checkbox" id="checkall" title="Select all" onClick="toggle(this)"/>
												</th><th>Identifiant du patient</th><th>Nom du patient</th><th>Date de naissance</th><th>Date d&apos;examen</th><th>Modalité</th><th>Type d&apos;examen</th>
											</tr></thead>';

											for ($i = 1; $i <= $nb_file; $i++) {

												$nom_file = 'rsp' . $i . '.dcm';

												require_once './libraries/nanodicom-master/nanodicom.php';
												$dicom = Nanodicom::factory('./dcm/' . $nom_file);
												$dicom -> parse(); // Parsing des fichiers avec Nanodicom

												if ($filtrage_type == 1) { // Si un filtrage sur le type a été indiqué, on le prend en comtpe :

													$string = $dicom -> value(0x0032, 0x1060);

													$bingo = FALSE;

													for ($j = 0; $j < $table_type_ligne; $j++) { // On vérifie si le(s) mot(s) indiqués dans le champ de filtrage appartiennent au type indiqué dans le fichier DICOM

														if (stristr($string, $table_type[$j]) === FALSE) {

														} else {
															$bingo = TRUE;
														}
													}

													if ($bingo === TRUE) { // S'il y a filtrage et que les mots demandés on été trouvé, on affichage les résultats correspondant

														echo '
														<tr>
															<td>
																<input name="nom_checkbox[]" type="checkbox" value="' . $nom_file . '" />
															</td><td> ' . $dicom -> value(0x0010, 0x0020) . '</td>
															<td>' . $dicom -> value(0x0010, 0x0010) . '</td>
															<td>' . dicom_to_date($dicom -> value(0x0010, 0x0030)) . '</td>
															<td>' . dicom_to_date($dicom -> value(0x0008, 0x0020)) . '</td>
															<td>' . $dicom -> value(0x0008, 0x0061) . '</td>
															<td>' . $dicom -> value(0x0032, 0x1060) . '</td>
														</tr>';

													} 


												} else { // S'il n'y a pas de filtrage, on affiche tout

													echo '
													<tr>
														<td>
															<input name="nom_checkbox[]" type="checkbox" value="' . $nom_file . '" />
														</td><td> ' . $dicom -> value(0x0010, 0x0020) . '</td>
														<td>' . $dicom -> value(0x0010, 0x0010) . '</td>
														<td>' . dicom_to_date($dicom -> value(0x0010, 0x0030)) . '</td>
														<td>' . dicom_to_date($dicom -> value(0x0008, 0x0020)) . '</td>
														<td>' . $dicom -> value(0x0008, 0x0061) . '</td>
														<td>' . $dicom -> value(0x0032, 0x1060) . '</td>
													</tr>';

												}

											}

											echo '
										</table>
										<br/>
										<br/>

										<input type="hidden" name="filecsv" value="filecsv" />
										<input type="hidden" name="filtrage_type"  value="' . $filtrage_type . '" />
										<input type="hidden" name="table_type"  value="' . htmlentities($table_type_serial) . '" /> 

										<input border=0 type="submit" name="series" value="Afficher les séries" class="myButton">
									</center>
								</form><br/>';

								if (isset($table_p) && !empty($table_p)) { // Si il existe des identifiants pour lesquels rien n'a été trouvé :
									echo "<h2>Aucun résultat n'a été trouvé pour les identifiants suivants : </h2><br/>
									<center><table class='result'>";

										for ($l = 0; $l <= count($table_p)-1; $l++) {
											echo '<tr><td>' . $table_p[$l] . '</td></tr>';
										}

										echo "</table></center>"; 

									}

								}
							} else { // Si l'upload du fichier a échoué :
								echo "Une erreur est survenue durant l'envoi du fichier vers le serveur.";
								echo $_FILES["file_csv"]["error"];
							}
						}
					}
				}
				?>

				<br/>
				<br/>
				<center>
					<a href="path_csv.php" class="myButton">Retour</a>
				</center>

			</div>
		</body>

		</html>