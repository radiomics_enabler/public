<?php
if(session_id()=='') 
	session_start();

if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="style/style.css" />

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/extensions/TableTools/css/dataTables.tableTools.css">

	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.js"></script>

	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/extensions/TableTools/js/dataTables.tableTools.js"></script>

	<title>Radiomics Enabler</title>

	<!-- Paramétrage de DataTables -->
	<script type="text/javascript">

		$(document).ready( function () {
			$('#result').DataTable( {
				"language": {
					"url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json" // Choix du fichier langue

				},
				"order": [ 2, 'asc' ], // Paramétrage du tri par défaut (tri ascendant sur colonne 2)
				"aoColumnDefs": [
				{ "bSortable": false, "aTargets": [ 0 ] } // On empeche le tri dynamique sur colonne 0 (cases à cocher)
				],
				aLengthMenu: [ // Choix possble dans le menu de taille du tableau
				[10,25, 50, 100, 200, -1],
				[10,25, 50, 100, 200, "Tous"]
				],



				dom: 'T<"clear">lfrtip',

				tableTools: { // Initialisation des fonctions d'export CSV / PDF
				"sSwfPath": "//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
				{
					"sExtends": "copy",
					"sButtonText": "Copier"
				},
				{
					"sExtends": "csv",
					"sButtonText": "Export CSV"
				},
				{
					"sExtends": "pdf",
					"sButtonText": "Export PDF"
				}
				]
			}

		}
		);

			var table = $('#result').DataTable();

			$('#selectall').click(function(event) { // Fonction qui selectionne toutes les checkbox, lors d'un clic sur la checkbox dans le header du tableau :

    			if(this.checked) { // Si on coche la checkbox dans le header
          			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
          			this.checked = true;  // On les coche    
          			$(this).closest("tr").addClass("selected"); // On passe la classe de DataTable a "selected", pour obtenir l'effet visuel de sélection               
          		});
          		}

          		else{ // Si on décoche la checkbox dans le header
           			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
           			this.checked = false; // On les decoche
           			$(this).closest("tr").removeClass("selected"); // On enlève la classe de DataTable "selected", pour enlever l'effet visuel de sélection 
           		});        
           		}
           	});

			$('#result tbody').on( 'click', ':checkbox', function() {
				$(this).closest("tr").toggleClass('selected');  // Lors du cochage/décochage individuel d'une checkbox, on active/désactive la classe "selected" pour gérer les effets visuels
			} );
		} );


</script>

</head>

<body>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<div class="content">

		<h1>Résultats de la recherche :</h1>

		<?php

		if (isset($_POST['retour_libre'])) { // Si on revient sur cette page, depuis la page qui suit : (sélection des séries)

			// On regénère le tableau, sans avoir à rééexcuter la reqûete :

			echo '
			<form id="ok" name="traitement" method="POST" action="traitement.php">
				<center><br/><br/>	
					<table id="result" class="display">
						<thead><tr>
							<th>
								<input type="checkbox" id="selectall" title="Select all"/>
							</th><th>Identifiant du patient</th><th>Nom du patient</th><th>Date de naissance</th><th>Date d&apos;examen</th><th>Modalité</th><th>Type d&apos;examen</th>
						</tr></thead><tbody>';

						$examens = glob('./dcm/*.{dcm}', GLOB_BRACE);
						foreach($examens as $file) {

							require_once './libraries/nanodicom-master/nanodicom.php';
							$dicom = Nanodicom::factory($file);
							$dicom -> parse(); 

							echo '
							<tr>
								<td>
									<input name="nom_checkbox[]" type="checkbox" class="case" id = "\'item.uid+\'_I" value="'.$file.'" />
								</td><td> ' . $dicom -> value(0x0010, 0x0020) . '</td>
								<td>' . $dicom -> value(0x0010, 0x0010) . '</td>
								<td>' . dicom_to_date($dicom -> value(0x0010, 0x0030)) . '</td>
								<td>' . dicom_to_date($dicom -> value(0x0008, 0x0020)) . '</td>
								<td>' . $dicom -> value(0x0008, 0x0061) . '</td>
								<td>' . $dicom -> value(0x0032, 0x1060) . '</td>
							</tr>';

						}

						

							// On stocke les variables importantes dans un formulaire caché, afin de les récupérer lors d'un changement de page

						echo '
					</tbody></table>
					<br/><br/>

					<input type="hidden" name="libre" value="libre"/>
					<input type="hidden" name="filtrage_type"  value="' . $filtrage_type . '" />
					<input type="hidden" name="table_type"  value="' . htmlentities($table_type_serial) . '" /> 

					<input border=0 type="submit" name="series" value="Afficher les séries" class="myButton">
				</center>
			</form>';
		} 

		else { // Si on accède à la page depuis le moteur de recherche, de manière "normale"

			// On récupère les critères de recherche, via POST

		$nom = $_POST["nom_patient"];
		$datenaissdeb = $_POST["datenaissdeb_patient"];
		$datenaissfin = $_POST["datenaissfin_patient"];
		$id = $_POST["id_patient"];
		$num_dos = $_POST["num_dos"];
		$datedeb = $_POST["datedeb"];
		$datefin = $_POST["datefin"];
		$type_ex = $_POST['type_ex'];
		$filtrage_type = 0;
		$modalite = "";
		$table_type = "";
		

		$table_verif = array(
			0  => "0",
			1  => "1",
			2  => "2",
			3  => "3",
			4  => "4",
			5  => "5",
			6  => "6",
			7  => "7",
			8  => "8",
			9  => "9",

			);

			if (isset($_POST['type_ex']) && !empty($_POST['type_ex'])) { // Si le champ type a été renseigné :

				$table_type = explode (";", $_POST['type_ex']);
				$table_type_ligne = count($table_type);
				$filtrage_type = 1;
			}

			$table_type_serial = serialize($table_type);

			if (isset($_POST['modalite']) && !empty($_POST['modalite'])) { // Si une ou des modalités ont été demandées :
				$ModArray = $_POST['modalite'];

				$a = count($ModArray);


				if ($a == 1) {
					$modalite = $ModArray[$a-1];

				} else {
					$modalite = $ModArray[0];
					for ($b = 1; $b < $a; $b++ ){
						$modalite = $modalite.'\\'.$ModArray[$b];
					}

				}

			}


			if (empty($datedeb) == FALSE) { // Si les dates de début et/ou de fin ont été renseignées, on les convertit au format DICOM (jj/mm/yyyy -> yyyymmjj)

				$datedeb = date_to_dicom($datedeb);

				if (empty($datefin) == FALSE) {

					$datefin = date_to_dicom($datefin);

				}

				remove_file("./dcm"); // Purge du dossier dcm
				$requete = 'cd ./dcm && sudo /usr/bin/findscu -X -S --aetitle PROL_QUERY_SCU --call ' . $dicom_aet . ' -k "(0008,0052)=STUDY"  -k "(0020,0010)=" -k "(0020,000D)="  -k "(0008,0020)=' . $datedeb . '-' . $datefin . '" -k "(0010,0010)=" -k "(0010,0020)=" -k "(0010,0030)=" -k "(0008,0061)=' . $modalite . '" -k "(0008,1010)=" -k "(0008,0080)=" -k "(0032,1060)=" ' . $dicom_server . ' ' . $dicom_port;

			} else { // Si l'utilisateur a choisi d'effectuer une recherche par patient :

			if (empty($datenaissdeb) == FALSE) {

				$datenaissdeb = date_to_dicom($datenaissdeb);
				$datenaissdeb = $datenaissdeb . '-';

			}

			if (empty($datenaissfin) == FALSE) {

				$datenaissfin = date_to_dicom($datenaissfin);
				$datenaissfin = $datenaissfin;

			}

				remove_file("./dcm"); // Purge du dossier dcm
				$requete = 'cd ./dcm && sudo /usr/bin/findscu -X -S --aetitle PROL_QUERY_SCU --call ' . $dicom_aet . ' -k "(0008,0052)=STUDY" -k "(0020,0010)=" -k "(0020,000D)="  -k "(0008,0061)=' . $modalite . '" -k "(0008,0080)=" -k "(0032,1060)=" -k "(0008,1010)=" -k "(0008,0020)=" -k "(0010,0010)=' . $nom . '" -k "(0010,0020)=' . $id . '" -k "(0010,0030)=' . $datenaissdeb . $datenaissfin . '" ' . $dicom_server . ' ' . $dicom_port;

			}

			exec($requete, $output); // Exécution de la requête

			$nb_file = sizeof(glob("./dcm/*.dcm" )); // On compte le nombre de fichiers dans le dossier DCM

			if ($nb_file == 0) { // Si il n'y a pas de fichiers :
			echo '<h2>Aucun résultat n&apos;a été trouvé pour vos paramètres de recherche</h2>';
		} else {

			echo '
			<form id="ok" name="traitement" method="POST" action="traitement.php">
				<center><br/><br/>	
					<table id="result" class="display">
						<thead><tr>
							<th>
								<input type="checkbox" id="selectall" title="Select all"/></th>
							</th><th>Identifiant du patient</th><th>Nom du patient</th><th>Date de naissance</th><th>Date d&apos;examen</th><th>Modalité</th><th>Type d&apos;examen</th>
						</tr></thead><tbody>';

						for ($i = 1; $i <= $nb_file; $i++) {

							if ($i < 10) {
								$nom_file = 'rsp000' . $i . '.dcm';
							} elseif (10 <= $i && $i < 100) {
								$nom_file = 'rsp00' . $i . '.dcm';
							} elseif (100 <= $i && $i < 1000) {
								$nom_file = 'rsp0' . $i . '.dcm';
							} elseif ($i >= 1000) {
								$nom_file = 'rsp' . $i . '.dcm';
							}

							require_once './libraries/nanodicom-master/nanodicom.php';

							$nom_file = './dcm/' . $nom_file;
							$dicom = Nanodicom::factory($nom_file);
								$dicom -> parse(); // On parse les fichiers DICOM à l'aide de l'utilitaire Nanodicom

								if ($filtrage_type == 1) { // Si un filtrage sur le type a été indiqué, on le prend en comtpe :

									$string = $dicom -> value(0x0032, 0x1060);

									for ($j = 0; $j < $table_type_ligne; $j++) { // On vérifie si le(s) mot(s) indiqués dans le champ de filtrage appartiennent au type indiqué dans le fichier DICOM

										if (stristr($string, $table_type[$j]) === FALSE) {
											unlink($nom_file);
										} else {
											echo '
											<tr>
												<td>
													<input name="nom_checkbox[]" type="checkbox" class="case" id = "\'item.uid+\'_I" value="'.$nom_file.'" />
												</td><td> ' . $dicom -> value(0x0010, 0x0020) . '</td>
												<td>' . $dicom -> value(0x0010, 0x0010) . '</td>
												<td>' . dicom_to_date($dicom -> value(0x0010, 0x0030)) . '</td>
												<td>' . dicom_to_date($dicom -> value(0x0008, 0x0020)) . '</td>
												<td>' . $dicom -> value(0x0008, 0x0061) . '</td>
												<td>' . $dicom -> value(0x0032, 0x1060) . '</td>
											</tr>';
										}
									}



								} else {
									echo '
									<tr>
										<td>
											<input name="nom_checkbox[]" type="checkbox" class="case" id = "\'item.uid+\'_I" value="' . $nom_file . '" />
										</td><td> ' . $dicom -> value(0x0010, 0x0020) . '</td>
										<td>' . $dicom -> value(0x0010, 0x0010) . '</td>
										<td>' . dicom_to_date($dicom -> value(0x0010, 0x0030)) . '</td>
										<td>' . dicom_to_date($dicom -> value(0x0008, 0x0020)) . '</td>
										<td>' . $dicom -> value(0x0008, 0x0061) . '</td>
										<td>' . $dicom -> value(0x0032, 0x1060) . '</td>
									</tr>';
								}

							}

							echo '
						</tbody></table>
						<br/><br/>

						<input type="hidden" name="libre" value="libre"/>
						<input type="hidden" name="filtrage_type"  value="' . $filtrage_type . '" />
						<input type="hidden" name="table_type"  value="' . htmlentities($table_type_serial) . '" />
						<input type="hidden" name="crit_nom" value="'.$nom.'"/>
						<input type="hidden" name="crit_datenaissdeb" value="'.$datenaissdeb.'"/>
						<input type="hidden" name="crit_datenaissfin" value="'.$datenaissfin.'"/>
						<input type="hidden" name="crit_datedeb" value="'.$datedeb.'"/>
						<input type="hidden" name="crit_datefin" value="'.$datefin.'"/>
						<input type="hidden" name="crit_id" value="'.$id.'"/>
						<input type="hidden" name="crit_modalite" value="'.$modalite.'"/>
						<input type="hidden" name="crit_type" value="'.$type_ex.'"/>
						<input type="hidden" name="crit_csv" value=""/>

						<input border=0 type="submit" name="series" value="Afficher les séries" class="myButton">
					</center>
				</form>';
			} 
		}
		echo "<script>";
		echo "document.getElementById('message').style.display = \"none\";";
		echo "</script>";
		?>

		<br/>
		<br/>
		<center>

			<form name="sauvegarder" id="save" method="POST" action="sauvegarde.php" accept-charset="UTF-8">
				<fieldset><br/>
					<legend>
						Sauvegarder la recherche en cours :
					</legend>
					Libellé de la recherche :
					<input type="text" size=30 name="lib_recherche"><span class="espace"/>
					Description :
					<input type="text" size=30 name="desc_recherche"><br/><br/>

					<?php

					echo '<input type="hidden" name="crit_nom" value="'.$nom.'"/>
					<input type="hidden" name="crit_datenaissdeb" value="'.$datenaissdeb.'"/>
					<input type="hidden" name="crit_datenaissfin" value="'.$datenaissfin.'"/>
					<input type="hidden" name="crit_datedeb" value="'.$datedeb.'"/>
					<input type="hidden" name="crit_datefin" value="'.$datefin.'"/>
					<input type="hidden" name="crit_id" value="'.$id.'"/>
					<input type="hidden" name="crit_modalite" value="'.$modalite.'"/>
					<input type="hidden" name="crit_type" value="'.$type_ex.'"/>
					<input type="hidden" name="type_save" value="STUDIES"/>
					<input type="hidden" name="crit_csv" value=""/>';

					?>

					<br/><br/><input border=0 type="submit" name="save" value="Sauvegarder la recherche" class="myButton"><br/><br/></fieldset>
				</form><br/>


				<a href="index.php" class="myButton">Retour</a>

			</center>

		</div>
	</body>
	</html>