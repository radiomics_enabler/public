<?php
if(session_id()=='') 
	session_start();

if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="style/style.css" />

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/extensions/TableTools/css/dataTables.tableTools.css">

	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.js"></script>

	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/extensions/TableTools/js/dataTables.tableTools.js"></script>

	<title>Radiomics Enabler</title>

	<!-- Paramétrage de DataTables -->
	<script type="text/javascript">

		$(document).ready( function () {
			$('#result').DataTable( {
				"language": {
					"url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json" // Choix du fichier langue

				},
				"order": [ 0, 'desc' ], // Paramétrage du tri par défaut (tri ascendant sur colonne 0)
				"aoColumnDefs": [
				{ "bSortable": false, "aTargets": [ 3 ] } // On empeche le tri dynamique sur colonne 4 et 5
				],
				aLengthMenu: [ // Choix possble dans le menu de taille du tableau
				[10,25, 50, 100, 200, -1],
				[10,25, 50, 100, 200, "Tous"]
				],



				dom: 'T<"clear">lfrtip',

				tableTools: { // Initialisation des fonctions d'export CSV / PDF
				"sSwfPath": "//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
				{
					"sExtends": "copy",
					"sButtonText": "Copier"
				},
				{
					"sExtends": "csv",
					"sButtonText": "Export CSV"
				},
				{
					"sExtends": "pdf",
					"sButtonText": "Export PDF"
				}
				]
			}

		}
		);

			var table = $('#result').DataTable();

			$('#selectall').click(function(event) { // Fonction qui selectionne toutes les checkbox, lors d'un clic sur la checkbox dans le header du tableau :

    			if(this.checked) { // Si on coche la checkbox dans le header
          			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
          			this.checked = true;  // On les coche    
          			$(this).closest("tr").addClass("selected"); // On passe la classe de DataTable a "selected", pour obtenir l'effet visuel de sélection               
          		});
          		}

          		else{ // Si on décoche la checkbox dans le header
           			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
           			this.checked = false; // On les decoche
           			$(this).closest("tr").removeClass("selected"); // On enlève la classe de DataTable "selected", pour enlever l'effet visuel de sélection 
           		});        
           		}
           	});

			$('#result tbody').on( 'click', ':checkbox', function() {
				$(this).closest("tr").toggleClass('selected');  // Lors du cochage/décochage individuel d'une checkbox, on active/désactive la classe "selected" pour gérer les effets visuels
			} );
		} );


</script>

</head>

<body>

	<?php menu($niveau); ?>

	<div class="content">

	<h1>Bienvenue <?php echo $_SESSION['prenom']." ".$_SESSION['nom']; ?>.</h1>

		<h1>Recherches sauvegardées :</h1>

		<center> <table id="result" class="display">
			<thead><tr>
				<th>Date de la recherche</th><th>Libellé de la recherche</th><th>Description de la recherche</th><th>Accéder à la recherche</th>
			</tr></thead><tbody>

			<?php

			include ('./inclusions/connect.inc');
			/* Modification du jeu de résultats en utf8 */
			if (!mysqli_set_charset($conn, "utf8")) {
			//printf("Erreur lors du chargement du jeu de caractères utf8 : %s\n", mysqli_error($link));
			} else {
			//printf("Jeu de caractères courant : %s\n", mysqli_character_set_name($conn));
			}

			$sql='SELECT searches.* FROM searches, users WHERE searches.id_user = users.id_user AND users.id_user = "'.$_SESSION["id_user"].'";';

			$result = mysqli_query($conn, $sql);

			if (mysqli_num_rows($result) > 0) {

				while($row = mysqli_fetch_assoc($result)) {

					$crit_datedeb = $row["dateb_search"];
					$crit_datefin = $row["datee_search"];
					$crit_type = $row["type_crit"];
					$crit_modalite = $row["modality_crit"];
					echo '<tr><form id="form" name="search" method="POST" action="review_search.php">
					<td>'.$row["date_search"].'</td>
					<td>'.$row["name_search"].'</td>
					<td><a class="infobulle">'.$row["desc_search"].'
					<span class="infobulle">
						<u>Critères de recherche : </u><br/>
						Date de début : '.$crit_datedeb.' <br/>
						Date de fin : '.$crit_datefin.' <br/>
						Modalité: '.$crit_modalite.' <br/>
						Type : '.$crit_type.' <br/>
						</span></a></td>
					<td><input type="hidden" name="id_search"  value="'.$row["id_search"].'" /><input type="submit" name="voir" value="Voir" class="myButton" ></td></form></tr>
					';

				}
			}
			mysqli_close($conn);

			?>
		</tbody>
	</table>
</center>
</div>
</body>
</html>