<?php
if(session_id()=='') 
	session_start();

if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style/style.css" />
	<title>Radiomics Enabler</title>
</head>

<body>

	<?php menu($niveau); ?>

	<div class="content">

		<?php

		//---- Purge du dossier "store" : ----//

		remove_file("./store");
		
		//---- Récupération des séries : ----//

		if ($_POST['send'] == 'Exporter les séries sélectionnées') {

			$cmd = "cd ./store && sudo /usr/bin/storescp -aet PROL_STORE_SCP 11112"; 
		exec($cmd . " > /dev/null &"); //Lancement du StoreSCP pour récupérer les fichiers (dans le dossier store)

		foreach ($_POST['nom_checkbox'] as $nom_file) {

			$requete1_1 = 'cd ./series_dcm && sudo /usr/bin/movescu -S --port 104 --aetitle PROL_RETR_SCU --move PROL_STORE_SCP --call ' . $dicom_aet . ' ' . $dicom_server . ' ' . $dicom_port .' '.$nom_file.' 2>&1 -v ';
			exec($requete1_1, $output);  //Exécution du C-MOVE, pour chaque fichier .DCM sélectionné

		}

		echo '<h1>Opération terminée</h1>';
		echo "<h3>Les fichiers sont maintenant accessibles dans le dossier partagé <b><i>Share</b></i>, pour export ou quantification.</h3><br/><br/>";
		
	} elseif ($_POST['send'] == 'Envoyer vers TeraRecon') {

		foreach ($_POST['nom_checkbox'] as $nom_file) {

			$requete1_1 = 'cd ./series_dcm && sudo /usr/bin/movescu -S --aetitle PROL_RETR_SCU --move AQNET01_AE --call ' . $dicom_aet . ' ' . $dicom_server . ' ' . $dicom_port .' '.$nom_file.' 2>&1 -v ';
			exec($requete1_1, $output);  //Exécution du C-MOVE, pour chaque fichier .DCM sélectionné

		}

		echo '<h1>Opération terminée</h1>';
		echo "<h3>Les fichiers sont maintenant accessibles depuis la console TeraRecon.</h3><br/><br/>";


	}

	elseif ($_POST['send'] == 'Envoyer vers OLEA') {

		foreach ($_POST['nom_checkbox'] as $nom_file) {

			$requete1_1 = 'cd ./series_dcm && sudo /usr/bin/movescu -S --aetitle PROL_RETR_SCU --move OLEASPHERE --call ' . $dicom_aet . ' ' . $dicom_server . ' ' . $dicom_port .' '.$nom_file.' 2>&1 -v ';
			exec($requete1_1, $output);  //Exécution du C-MOVE, pour chaque fichier .DCM sélectionné

		}

		echo '<h1>Opération terminée</h1>';
		echo "<h3>Les fichiers sont maintenant accessibles depuis la console OLEA.</h3><br/><br/>";
	}

	?>

	<p><a href="./index.php" class="myButton">Retour</a></p>

</div>

</body>

</html>