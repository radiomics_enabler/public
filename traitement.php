<?php
if(session_id()=='') 
	session_start();

if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style/style.css" />

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="./libraries/data_table/media/css/jquery.dataTables.css">

	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.js"></script>

	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="./libraries/data_table/media/js/jquery.dataTables.js"></script>

	<title>Radiomics Enabler</title>

	<!-- Paramétrage de DataTables -->
	<script type="text/javascript">

		$(document).ready( function () { 

			var table = $('#result').DataTable( { 

				"order": [[ 7, 'asc' ],[ 6, 'asc' ]], // Paramétrage du tri par défaut (tri ascendant sur colonne 7 et colonne 6)

				"language": {
					"url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json" // Choix du fichier langue
				},

				"aoColumnDefs": [ // On empeche le tri dynamique sur colonnes 0 (cases à cocher), 5 et 6 + on rend invisible la colonne 7 (utilisée pour le regroupage)
				{ "bSortable": false, "aTargets": [ 0 ] },
				{ "bSortable": false, "aTargets": [ 5 ] },
				{ "bSortable": false, "aTargets": [ 6 ] },
				{ "bVisible": false, "aTargets": [ 7 ] }
				],
				aLengthMenu: [ // Choix possble dans le menu de taille du tableau
				[10, 25, 50, 100, 200, -1],
				[10, 25, 50, 100, 200, "Tous"]
				],

				"drawCallback": function ( settings ) { // Fonction de regroupage des séries pour un même examen :
					var api = this.api();
					var rows = api.rows( {page:'current'} ).nodes();
					var last=null;

					api.column(7, {page:'current'} ).data().each( function ( group, i ) {
						if ( last !== group ) {
							$(rows).eq( i ).before(
								'<tr class="group"><td colspan="5">'+group+'</td></tr>'
								);

							last = group;
						}
					} );
				}
			} );


		//--------------------------------------
		var table = $('#result').DataTable();

		$('#selectall').click(function(event) { // Fonction qui selectionne toutes les checkbox, lors d'un clic sur la checkbox dans le header du tableau :

    			if(this.checked) { // Si on coche la checkbox dans le header
          			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
          			this.checked = true;  // On les coche   
          			$(this).closest("tr").addClass("selected"); // On passe la classe de DataTable a "selected", pour obtenir l'effet visuel de sélection                
          		});
          		}

          		else{ // Si on décoche la checkbox dans le header
           			$('.case' , table.rows().nodes()).each(function() { // On parcours chaque checkbox
           			this.checked = false; // On les decoche
           			$(this).closest("tr").removeClass("selected"); // On enlève la classe de DataTable "selected", pour enlever l'effet visuel de sélection 
           		});        
           		}
           	});

		$('#result tbody').on( 'click', ':checkbox', function() {
			$(this).closest("tr").toggleClass('selected'); // Lors du cochage/décochage individuel d'une checkbox, on active/désactive la classe "selected" pour gérer les effets visuels
		} );

	} );


</script>

</head>
<body>

	<div class="loader"></div>

	<!-- Inclusion menu principal -->
	<?php menu($niveau); ?>

	<?php

	$nom = $_POST["nom_patient"];
	$datenaissdeb = $_POST["datenaissdeb_patient"];
	$datenaissfin = $_POST["datenaissfin_patient"];
	$id = $_POST["id_patient"];
	$num_dos = $_POST["num_dos"];
	$datedeb = $_POST["datedeb"];
	$datefin = $_POST["datefin"];
	$type_ex = $_POST['type_ex'];
	$filtrage_type = $_POST["filtrage_type"];
	$table_type_serial = $_POST["table_type"];

	$table_verif = array(
		0  => "0",
		1  => "1",
		2  => "2",
		3  => "3",
		4  => "4",
		5  => "5",
		6  => "6",
		7  => "7",
		8  => "8",
		9  => "9",

		);


	echo '<div class="content">

	<h1>Affichage des séries</h1>

	<center><form id="ok" name="send" method="POST" action="envoi_series.php">
		<table id="result" class="display">
			<thead><tr>
				<th><input type="checkbox" id="selectall" title="Select all"/></th><th>Identifiant du patient</th><th>Nom du patient</th><th>Date de naissance</th><th>Date examen</th><th>Modalité</th><th>Type</th><th>Tri</th>
			</tr></thead>';

			if (isset($_POST['nom_checkbox'])) { // On récupère les nom des checkbox sélectionnées à l'écran d'avant, qui correspondent aux noms des fichiers DCM des examens choisis

			$d = 1;
				remove_file("./series_dcm"); // On purge le dossier series_dcm

				foreach ($_POST['nom_checkbox'] as $nom_file) {
					require_once './libraries/nanodicom-master/nanodicom.php';
					$dicom = Nanodicom::factory($nom_file);
					$dicom -> parse(); // On parse les fichiers DCM correspondant aux examens choisis

					$id_series = $dicom -> value(0x0020, 0x000d); // On récupère l'ID

					$count_id_series = strlen($id_series);

					$last_char = substr($id_series, -1, 1);

					$intrus = TRUE;

					for ($f = 0; $f < 10; $f++) {

						if (stristr($last_char, $table_verif[$f]) === FALSE) {

						} else {
							$intrus = FALSE;
						}
					}

					if ($intrus === TRUE) {
						$id_series = substr($id_series,0,-1);
					}			

					remove_file("./temp"); // On purge le dossier temp

					$requete2 = 'cd ./temp && sudo /usr/bin/findscu -X -S --aetitle PROL_QUERY_SCU --call ' . $dicom_aet . ' -k "(0008,0052)=SERIES"  -k "(0020,000d)='.$id_series.'" -k "(0020,000e)=" -k "(0010,0010)=" -k "(0010,0020)=" -k "(0010,0030)=" -k "(0008,0020)=" -k "(0032,1060)=" -k "(0008,0060)=" -k "(0008,103e)=" ' . $dicom_server . ' ' . $dicom_port;
					exec($requete2, $output); // On effectue une recherche selon l'ID de l'examen récupéré

					$nb_dcm = sizeof(glob("./temp/*.dcm" )); // On compte le nombre de fichiers reçus dans temp

					for ($i = 1; $i <= $nb_dcm; $i++) {

						if ($i < 10) {
							$nom_serie = 'rsp000' . $i . '.dcm';
						} elseif (10 <= $i && $i < 100) {
							$nom_serie = 'rsp00' . $i . '.dcm';
						} elseif (100 <= $i && $i < 1000) {
							$nom_serie = 'rsp0' . $i . '.dcm';
						} elseif ($i >= 1000) {
							$nom_serie = 'rsp' . $i . '.dcm';
						}

						$requete4 = 'cd ./temp && sudo cp ' . $nom_serie . ' ../series_dcm/rsp' . $d . '.dcm'; // On déplace ces fichiers dans series_dcm, en les renommant
						exec($requete4, $output);
						$d++;

					}

				}

				$nb_file = sizeof(glob("./series_dcm/*.dcm" )); // On compte combien de fichiers ont été déplacés dans series_dcm 

				if ($nb_file == 0) { // Si aucun fichier ne se trouve dans le dossier :
					echo '<h2>Aucun résultat n&apos;a été trouvé pour vos paramètres de recherche</h2>';
				} else {

					for ($i = 1; $i <= $nb_file; $i++) {

						$nom_file = 'rsp' . $i . '.dcm';

						$dicom = Nanodicom::factory('./series_dcm/' . $nom_file);
						$dicom -> parse(); // On parse chaque fichier reçu, et on récupère les informations voulues :

						$identifiant_patient = $dicom -> value(0x0010, 0x0020);
						$nom_patient = $dicom -> value(0x0010, 0x0010);
						$date_naiss = $dicom -> value(0x0010, 0x0030);
						$date_exam = $dicom -> value(0x0008, 0x0020);
						$type = $dicom -> value(0x0032, 0x1060);

						echo ' <tr><td> ';



						echo '<input name="nom_checkbox[]" type="checkbox" class="case" id = "\'item.uid+\'_I" value="' . $nom_file . '" />'; // On affiche les séries dans un tableau


						echo ' 	</td><td><i> ' . $identifiant_patient . '</i></td>	
						<td><i>' . $nom_patient . '</i></td>			
						<td><i>' . dicom_to_date($date_naiss) . '</i></td>
						<td><i>' . dicom_to_date($date_exam) . '</i></td>
						<td><i>' . $dicom -> value(0x0008, 0x0060) . '</i></td>
						<td><i>' . $dicom -> value(0x0008, 0x103e) . '</i></td>
						<td><h3><b>' . $nom_patient . '</b> --- <b>' . $type . '</b> --- <i>'.$id_series.'</i></h3></td>
					</tr>';

				}

			}



		}

		echo '</table><br/><br/><input border=0 type="submit" name="send" value="Exporter les séries sélectionnées" class="myButton"><span class="espace"/>';

		echo '<input border=0 type="submit" name="send" value="Envoyer vers OLEA" class="myButton"><span class="espace"/>';

		echo '<input border=0 type="submit" name="send" value="Envoyer vers TeraRecon" class="myButton"></form><br/><br/><br/><br/><br/>'; ?>


		<form name="sauvegarder" id="save" method="POST" action="sauvegarde.php" accept-charset="UTF-8">
				<fieldset><br/>
					<legend>
						Sauvegarder la recherche en cours :
					</legend>
					Libellé de la recherche :
					<input type="text" size=30 name="lib_recherche"><span class="espace"/>
					Description :
					<input type="text" size=30 name="desc_recherche"><br/><br/>

					<?php

					echo '<input type="hidden" name="crit_nom" value="'.$nom.'"/>
					<input type="hidden" name="crit_datenaissdeb" value="'.$datenaissdeb.'"/>
					<input type="hidden" name="crit_datenaissfin" value="'.$datenaissfin.'"/>
					<input type="hidden" name="crit_datedeb" value="'.$datedeb.'"/>
					<input type="hidden" name="crit_datefin" value="'.$datefin.'"/>
					<input type="hidden" name="crit_id" value="'.$id.'"/>
					<input type="hidden" name="crit_modalite" value="'.$modalite.'"/>
					<input type="hidden" name="crit_type" value="'.$type_ex.'"/>
					<input type="hidden" name="type_save" value="SERIES"/>
					<input type="hidden" name="crit_csv" value=""/>';

					?>

					<br/><br/><input border=0 type="submit" name="save" value="Sauvegarder la recherche" class="myButton"><br/><br/></fieldset>
				</form><br/>

		<?php



		if (isset($_POST['filecsv'])) { // Si on est arrivés sur cette page depuis la page de recherche CSV, le bouton retour nous y ramène

			echo '<center><form id="retour2" name="retour2" method="POST"  action="find_csv.php">
			<input type="hidden" name="filtrage_type"  value="' . $filtrage_type . '" />
			<input type="hidden" name="table_type"  value="' . htmlentities($table_type_serial) . '" />
			<input type="submit" name="retour_csv" value="Retour" class="myButton">
		</form></center>';

	}

	if (isset($_POST['libre'])) { // Si on est arrivés sur cette page depuis la page de recherche libre, le bouton retour nous y ramène

		echo '<center><form id="retour2" name="retour" method="POST" action="find_patient.php">
		<input type="hidden" name="filtrage_type"  value="' . $filtrage_type . '" />
		<input type="hidden" name="table_type"  value="' . htmlentities($table_type_serial) . '" />
		<input type="submit" name="retour_libre" value="Retour" class="myButton" >
	</form></center>';

}

?>

</body>

</html>