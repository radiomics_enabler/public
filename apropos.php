<?php
if(session_id()=='') 
	session_start();

if(!isSet($_SESSION['niv_cpte']))
{
	header("Location: ./login.php");
}
?>

<?php
include ('./inclusions/menu.php');
include ('./inclusions/fonction_date.inc');
include ('./inclusions/dicom_server.inc');
include ('./inclusions/remove.inc');
$niveau = 'Principal';
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style/style.css" />
	<title>Radiomics Enabler</title>
</head>

<body>

	<?php menu($niveau); ?>

	<div class="content">

		<h1>A propos du logiciel</h1><br/>

		Logiciel conçu par M. Guillaume SERRES, dans le cadre d'un stage de fin d'études de l'école d'ingénieurs ISIS (Informatique et Systèmes d'Information pour la Santé). <br/><br>

		Propriété du CHU de Toulouse, tous droits réservés. <br/><br/>

		Cette application utilise l'utilitaire DCMTK, ainsi que les bibliothèques DataTables, Nanodicom, Chosen, et CustomAlert. <br><br/>

		<br/><br/>

		<p><a href="./index.php" class="myButton">Retour</a></p>

	</div>

</body>

</html>